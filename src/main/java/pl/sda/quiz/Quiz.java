package pl.sda.quiz;

import java.io.*;
import java.util.*;

public class Quiz {

    public static void playQuiz() throws IOException {
        FileReader fileReader=null;
        File folder =new File("src\\main\\resources\\");
        File[] listOfFiles=folder.listFiles();


        Map<String,Map<String,ArrayList<String>>> mapOfCategories = new HashMap();
        int score=0;

//        int categoryNumber=1;
        for (File file:listOfFiles){
            if(file.isFile()){
                Map<String,ArrayList<String>> mapOfQuestions=new HashMap<String, ArrayList<String>>();
                fileReader=new FileReader(file);
                BufferedReader br=new BufferedReader(fileReader);
                String line;
                while ((line=br.readLine())!=null){

                    ArrayList<String> listOfQuestions=new ArrayList<String>();
                    int numberOfQuestions=Integer.parseInt(br.readLine());
                    for (int i = 0; i <numberOfQuestions ; i++) {
                     listOfQuestions.add(br.readLine());
                    }
                    mapOfQuestions.put(line,listOfQuestions);

                }
                mapOfCategories.put(file.getName().replace(".txt",""),mapOfQuestions);

            br.close();
            }

        }
        String [] arrayOfCategories=mapOfCategories.keySet().toArray((new String[mapOfCategories.keySet().size()]));
        int numberOfCategories=mapOfCategories.keySet().size();
        for (int i = 0; i < numberOfCategories; i++) {
            System.out.println((i+1)+" "+arrayOfCategories[i]);

        }
        System.out.println("CHOOSE A CATEGORY NUMBER ");
        Scanner scanner=new Scanner(System.in);
        Random random=new Random();
        String category = arrayOfCategories[Integer.parseInt(scanner.nextLine())-1];
        for (int i = 1; i <11 ; i++) {

            System.out.println("\nQUESTION "+i);
            String[] arrayOfKeys= mapOfCategories.get(category).keySet().
                    toArray(new String[mapOfCategories.get(category).size()]);
            int randomQuestionNumber=random.nextInt(arrayOfKeys.length);
            String question=arrayOfKeys[randomQuestionNumber];
            System.out.println(question);
            System.out.println("POSSIBLE ANSWERS:");
            ArrayList<String> listOfAnswers=mapOfCategories.get(category).get(question);

            String correctAnswer=listOfAnswers.get(0);
            Collections.shuffle(listOfAnswers);
            for (int j = 0; j <listOfAnswers.size() ; j++) {
                System.out.println((j+1)+" "+listOfAnswers.get(j));
            }



            System.out.println("TYPE ONE CORRECT ANSWER NUMBER");
            String answer=scanner.nextLine();
            if (listOfAnswers.get(Integer.parseInt(answer)-1).equals(correctAnswer)){
                System.out.println("WELL DONE!");
                score++;
            }else {
                System.out.println("SHAME OF YOU");
            }
        }

        System.out.println("YOUR SCORE: "+score+"/10");

    }
}
